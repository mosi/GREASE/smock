package org.mosi.grease.smock.logic.sstl;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mosi.grease.smock.TestUtils;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.FormulaSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.PatternSpecfication;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.RegionSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.TransformationSpecification;
import org.mosi.grease.smock.model.Point;
import org.mosi.grease.smock.model.patternelement.Formula;
import org.mosi.grease.smock.model.patternelement.Region;
import org.mosi.grease.smock.model.patternelement.Transformation;
import org.mosi.grease.smock.model.shape.Circle;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SSTLFormulaBuilderTest {


    @Test
    void yamlToCircleRegionTransformingWithoutKeyframesTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-circle-without-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Region region = spec.translateToObject();
        Assert.assertNotNull("Region is null", region);
        Assert.assertEquals("Region name should be \"compartment1\"", region.getName(), "compartment1");
        TestUtils.assertKeyFrames(region.getKeyframeMap());
        Assert.assertEquals("There should be 2 keyframes", 2, region.getKeyframeMap().size());
        Circle c1 = (Circle) region.getKeyframeMap().get(0d).getShape();
        Assert.assertEquals(c1.getCenter(), new Point(1349.5, 2249.5));
        Assert.assertEquals(400, c1.getRadius(), 0.0);
    }


    @Test
    void yamlToCircleRegionTransformingWithKeyframesTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-circle-with-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Region region = spec.translateToObject();
        Assert.assertNotNull("Region is null", region);
        Assert.assertEquals("Region name should be \"compartment1\"", region.getName(), "compartment1");
        TestUtils.assertKeyFrames(region.getKeyframeMap());
        Assert.assertEquals("There should be 2 keyframes", 2, region.getKeyframeMap().size());
        Circle c1 = (Circle) region.getKeyframeMap().get(0d).getShape();
        Assert.assertEquals(c1.getCenter(), new Point(1349.5, 2249.5));
        Assert.assertEquals(400, c1.getRadius(), 0.0);
        Circle c2 = (Circle) region.getKeyframeMap().get(20000000d).getShape();
        Assert.assertEquals(c2.getCenter(), new Point(1349.5, 2249.5));
        Assert.assertEquals(500, c2.getRadius(), 0.0);

    }


    @Test
    void yamlToRectangleRegionTransformingWithoutKeyframesTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-rectangle-without-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Region region = spec.translateToObject();
        Assert.assertNotNull("Region is null", region);
        Assert.assertEquals("Region name should be \"compartment1\"", region.getName(), "compartment1");
        TestUtils.assertKeyFrames(region.getKeyframeMap());
    }


    @Test
    void yamlToRectangleRegionTransformingWithKeyframesTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-rectangle-with-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Region region = spec.translateToObject();
        Assert.assertNotNull("Region is null", region);
        Assert.assertEquals("Region name should be \"compartment1\"", region.getName(), "compartment1");
        TestUtils.assertKeyFrames(region.getKeyframeMap());
    }


    @Test
    void yamlToSquareRegionTransformingWithKeyframesTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-square-with-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Region region = spec.translateToObject();
        Assert.assertNotNull("Region is null", region);
        Assert.assertEquals("Region name should be \"compartment1\"", region.getName(), "compartment1");
        TestUtils.assertKeyFrames(region.getKeyframeMap());
    }


    @Test
    void yamlToTransformationTransformingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader().getResource("patterns/test-transformation.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(TransformationSpecification.class));
        TransformationSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
        Transformation transformation = spec.translateToObject();
        Assert.assertNotNull("Transformation is null", transformation);
    }


    @Test
    void yamlToTransformationsTransformingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-transformations.yaml")).getFile();
        Constructor patternSpecificationConstructor = new Constructor(PatternSpecfication.class);
        TypeDescription typeDescription = new TypeDescription(PatternSpecfication.class);
        typeDescription.addPropertyParameters("transformations", TransformationSpecification.class);
        patternSpecificationConstructor.addTypeDescription(typeDescription);
        Yaml yaml = new Yaml(patternSpecificationConstructor);
        PatternSpecfication spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
        Assert.assertNull(spec.getFormulas());
        Assert.assertNotNull(spec.getTransformations());
        Assert.assertNull(spec.getRegions());
        List<Transformation> transformationList = spec.getTransformations()
                                                      .stream()
                                                      .map(TransformationSpecification::translateToObject)
                                                      .collect(Collectors.toList());
        Assert.assertTrue(transformationList.stream().allMatch(Objects::nonNull));
    }


    @Test
    void yamlToFormulaTransformingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader().getResource("patterns/test-formula.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(FormulaSpecification.class));
        FormulaSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Formula formula = spec.translateToObject();
        Assert.assertNotNull("Formula is null", formula);
    }

    @Test
    void yamlToFormulasTransformingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader().getResource("patterns/test-formulas.yaml"))
                                 .getFile();
        Constructor patternSpecificationConstructor = new Constructor(PatternSpecfication.class);
        TypeDescription typeDescription = new TypeDescription(PatternSpecfication.class);
        typeDescription.addPropertyParameters("formulas", FormulaSpecification.class);
        patternSpecificationConstructor.addTypeDescription(typeDescription);
        Yaml yaml = new Yaml(patternSpecificationConstructor);
        PatternSpecfication spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
        Assert.assertNotNull(spec.getFormulas());
        Assert.assertNull(spec.getTransformations());
        Assert.assertNull(spec.getRegions());
        List<Formula> formulas = spec.getFormulas()
                                     .stream()
                                     .map(FormulaSpecification::translateToObject)
                                     .collect(Collectors.toList());
        Assert.assertTrue(formulas.stream().allMatch(Objects::nonNull));
    }

}
