package org.mosi.grease.smock.elements.normalizer;


import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mosi.grease.smock.elements.particledataparser.ParticleDataParser;
import org.mosi.grease.smock.elements.particledataparser.csv.CSVParticleDataParser;
import org.mosi.grease.smock.elements.patternparser.PatternParser;
import org.mosi.grease.smock.elements.patternparser.yaml.YAMLPatternParser;
import org.mosi.grease.smock.model.NormalizedRepresentation;
import org.mosi.grease.smock.model.ParticleData;
import org.mosi.grease.smock.model.Pattern;
import org.mosi.grease.smock.model.scale.LinearScale;

import java.io.File;
import java.util.Objects;

public class LinearPatternNormalizerTest {
    Pattern pattern;
    ParticleData particleData;


    @BeforeEach
    public void parseData() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("testdata/data.csv"))
                                 .getFile();
        File testFile = new File(testPath);
        ParticleDataParser particleDataParser = new CSVParticleDataParser();
        this.particleData = particleDataParser.parseParticleData(testFile);
    }

    @BeforeEach
    public void parsePattern() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/pattern-specification.yaml")).getFile();
        File testFile = new File(testPath);
        PatternParser patternParser = new YAMLPatternParser();
        this.pattern = patternParser.parsePattern(testFile);
    }

    @Test
    public void normalizeDataTest() {
        PatternNormalizer patternNormalizer = new PatternNormalizer(new LinearScale());
        NormalizedRepresentation normalizedRepresentation = patternNormalizer.normalize(particleData, pattern.getRegions());
        Assert.assertNotNull("Normalized representation should not be null", normalizedRepresentation);
        Assert.assertNotNull("Particles inside the resolved regions should not be null", normalizedRepresentation.getResolvedRegionsParticleMap());
        Assert.assertNotNull("Resolved regions should not be null", normalizedRepresentation.getObservationTimeResolvedRegionsMap());


    }

}
