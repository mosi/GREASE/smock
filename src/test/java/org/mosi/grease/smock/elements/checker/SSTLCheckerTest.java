package org.mosi.grease.smock.elements.checker;


import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mosi.grease.smock.elements.generator.PatternGenerator;
import org.mosi.grease.smock.elements.normalizer.PatternNormalizer;
import org.mosi.grease.smock.elements.particledataparser.ParticleDataParser;
import org.mosi.grease.smock.elements.particledataparser.csv.CSVParticleDataParser;
import org.mosi.grease.smock.elements.patternparser.PatternParser;
import org.mosi.grease.smock.elements.patternparser.yaml.YAMLPatternParser;
import org.mosi.grease.smock.elements.transformer.PatternTransformer;
import org.mosi.grease.smock.logic.sstl.SSTLPatternGenerator;
import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;
import org.mosi.grease.smock.model.*;
import org.mosi.grease.smock.model.scale.LinearScale;

import java.io.File;
import java.util.Objects;

public class SSTLCheckerTest {
    private Pattern pattern;
    private ParticleData particleData;


    @BeforeEach
    public void parseData() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("testdata/data.csv"))
                                 .getFile();
        File testFile = new File(testPath);
        ParticleDataParser particleDataParser = new CSVParticleDataParser();
        this.particleData = particleDataParser.parseParticleData(testFile);
    }

    @BeforeEach
    public void parsePattern() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/pattern-specification.yaml")).getFile();
        File testFile = new File(testPath);
        PatternParser patternParser = new YAMLPatternParser();
        this.pattern = patternParser.parsePattern(testFile);
    }

    @Test
    public void checkSSTL() {
        PatternNormalizer patternNormalizer = new PatternNormalizer(new LinearScale());
        PatternTransformer patternTransformer = new PatternTransformer();
        PatternGenerator<SSTLRepresentation> patternGenerator = new SSTLPatternGenerator();
        PatternChecker<SSTLRepresentation> patternChecker = new SSTLPatternChecker();

        NormalizedRepresentation normalizedRepresentation = patternNormalizer.normalize(particleData, pattern.getRegions());
        TransformedRepresentation transformedRepresentation = patternTransformer.transform(normalizedRepresentation, pattern
                .getTransformations());
        SSTLRepresentation sstlRepresentation = patternGenerator.generate(transformedRepresentation, pattern);
        CheckingResult checkingResult = patternChecker.checkResult(sstlRepresentation, pattern);
        checkingResult.report();
        Assert.assertNotNull(checkingResult);
    }

}
