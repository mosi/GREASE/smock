package org.mosi.grease.smock.elements.particledataparser;

import org.junit.jupiter.api.Test;
import org.mosi.grease.smock.elements.particledataparser.csv.CSVParticleDataParser;

import java.io.File;
import java.util.Objects;

public class CSVParticleDataParserTest {

    @Test
    void parserTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("testdata/data.csv"))
                                 .getFile();
        File testFile = new File(testPath);
        ParticleDataParser particleDataParser = new CSVParticleDataParser();
        particleDataParser.parseParticleData(testFile);
    }
}