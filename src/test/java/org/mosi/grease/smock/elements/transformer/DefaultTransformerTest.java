package org.mosi.grease.smock.elements.transformer;


import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mosi.grease.smock.elements.normalizer.PatternNormalizer;
import org.mosi.grease.smock.elements.particledataparser.ParticleDataParser;
import org.mosi.grease.smock.elements.particledataparser.csv.CSVParticleDataParser;
import org.mosi.grease.smock.elements.patternparser.PatternParser;
import org.mosi.grease.smock.elements.patternparser.yaml.YAMLPatternParser;
import org.mosi.grease.smock.model.NormalizedRepresentation;
import org.mosi.grease.smock.model.ParticleData;
import org.mosi.grease.smock.model.Pattern;
import org.mosi.grease.smock.model.TransformedRepresentation;
import org.mosi.grease.smock.model.scale.LinearScale;

import java.io.File;
import java.util.Objects;

public class DefaultTransformerTest {
    Pattern pattern;
    ParticleData particleData;


    @BeforeEach
    public void parseData() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("testdata/data.csv"))
                                 .getFile();
        File testFile = new File(testPath);
        ParticleDataParser particleDataParser = new CSVParticleDataParser();
        this.particleData = particleDataParser.parseParticleData(testFile);
    }

    @BeforeEach
    public void parsePattern() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/pattern-specification.yaml")).getFile();
        File testFile = new File(testPath);
        PatternParser patternParser = new YAMLPatternParser();
        this.pattern = patternParser.parsePattern(testFile);
    }

    @Test
    public void transformData() {
        PatternNormalizer patternNormalizer = new PatternNormalizer(new LinearScale());
        PatternTransformer patternTransformer = new PatternTransformer();
        NormalizedRepresentation normalizedRepresentation = patternNormalizer.normalize(particleData, pattern.getRegions());
        TransformedRepresentation transformedRepresentation = patternTransformer.transform(normalizedRepresentation, pattern
                .getTransformations());
        Assert.assertNotNull("Transformed pattern should not be null", transformedRepresentation);
    }

}
