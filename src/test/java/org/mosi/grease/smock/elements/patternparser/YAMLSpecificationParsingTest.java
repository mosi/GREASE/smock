package org.mosi.grease.smock.elements.patternparser;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mosi.grease.smock.TestUtils;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.FormulaSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.PatternSpecfication;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.RegionSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.TransformationSpecification;
import org.mosi.grease.smock.model.patternelement.Region;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Objects;

public class YAMLSpecificationParsingTest {
    @Test
    void yamlToCircleRegionSpecificationWithKeyframesParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-circle-with-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
    }

    @Test
    void yamlToRectangleRegionSpecificationWithoutKeyframesParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-rectangle-without-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
    }

    @Test
    void yamlToRectangleRegionSpecificationWithKeyframesParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-rectangle-with-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
    }

    @Test
    void yamlToSquareRegionSpecificationWithoutKeyframesParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-square-without-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
    }


    @Test
    void yamlToTransformationSpecificationParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader().getResource("patterns/test-transformation.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(TransformationSpecification.class));
        TransformationSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
    }

    @Test
    void yamlToCircleRegionSpecificationWithoutKeyframesParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-circle-without-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
    }

    @Test
    void yamlToSquareRegionSpecificationWithKeyframesParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-square-with-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
    }


    @Test
    void yamlToSquareRegionSpecificationParsingWithoutKeyframesTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-region-square-without-keyframes.yaml"))
                                 .getFile();
        Yaml yaml = new Yaml(new Constructor(RegionSpecification.class));
        RegionSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Region region = spec.translateToObject();
        Assert.assertNotNull("Region is null", region);
        Assert.assertEquals("Region name should be \"compartment1\"", region.getName(), "compartment1");
        TestUtils.assertKeyFrames(region.getKeyframeMap());
    }


    @Test
    void yamlToRegionsSpecificationParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader().getResource("patterns/test-regions.yaml"))
                                 .getFile();
        Constructor patternSpecificationConstructor = new Constructor(PatternSpecfication.class);
        TypeDescription typeDescription = new TypeDescription(PatternSpecfication.class);
        typeDescription.addPropertyParameters("regions", RegionSpecification.class);
        typeDescription.addPropertyParameters("transformations", TransformationSpecification.class);
        typeDescription.addPropertyParameters("formulas", FormulaSpecification.class);
        patternSpecificationConstructor.addTypeDescription(typeDescription);
        Yaml yaml = new Yaml(patternSpecificationConstructor);
        PatternSpecfication spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
        Assert.assertNull(spec.getFormulas());
        Assert.assertNull(spec.getTransformations());
        Assert.assertNotNull(spec.getRegions());
    }

    @Test
    void yamlToTransformationsSpecificationParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-transformations.yaml")).getFile();
        Constructor patternSpecificationConstructor = new Constructor(PatternSpecfication.class);
        TypeDescription typeDescription = new TypeDescription(PatternSpecfication.class);
        typeDescription.addPropertyParameters("transformations", TransformationSpecification.class);
        patternSpecificationConstructor.addTypeDescription(typeDescription);
        Yaml yaml = new Yaml(patternSpecificationConstructor);
        PatternSpecfication spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
        Assert.assertNull(spec.getFormulas());
        Assert.assertNotNull(spec.getTransformations());
        Assert.assertNull(spec.getRegions());
    }

    @Test
    void yamlToFormulaSpecificationParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-formula.yaml")).getFile();
        Yaml yaml = new Yaml(new Constructor(FormulaSpecification.class));
        FormulaSpecification spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
    }

    @Test
    void yamlToFormulasSpecificationParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/test-formulas.yaml")).getFile();
        Constructor patternSpecificationConstructor = new Constructor(PatternSpecfication.class);
        TypeDescription typeDescription = new TypeDescription(PatternSpecfication.class);
        typeDescription.addPropertyParameters("transformations", TransformationSpecification.class);
        patternSpecificationConstructor.addTypeDescription(typeDescription);
        Yaml yaml = new Yaml(patternSpecificationConstructor);
        PatternSpecfication spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
        Assert.assertNotNull(spec.getFormulas());
        Assert.assertNull(spec.getTransformations());
        Assert.assertNull(spec.getRegions());
    }
}
