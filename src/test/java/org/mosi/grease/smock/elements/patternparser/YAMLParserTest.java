package org.mosi.grease.smock.elements.patternparser;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mosi.grease.smock.elements.patternparser.yaml.YAMLPatternParser;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.FormulaSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.PatternSpecfication;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.RegionSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.TransformationSpecification;
import org.mosi.grease.smock.model.Pattern;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Objects;

public class YAMLParserTest {
    @Test
    void yamlPatternParserTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/pattern-specification.yaml")).getFile();
        File testFile = new File(testPath);
        PatternParser patternParser = new YAMLPatternParser();
        Pattern pattern = patternParser.parsePattern(testFile);
        Assert.assertNotNull("Pattern is null", pattern);
        Assert.assertNotNull("Formulas are null", pattern.getFormulas());
        Assert.assertNotNull("Regions are null", pattern.getRegions());
        Assert.assertNotNull("Transformations are null", pattern.getTransformations());
        Assert.assertTrue("There is a null formula", pattern.getFormulas().stream().allMatch(Objects::nonNull));
        Assert.assertTrue("There is a null region", pattern.getRegions().stream().allMatch(Objects::nonNull));
        Assert.assertTrue("There is a null transformation", pattern.getTransformations()
                                                                   .stream()
                                                                   .allMatch(Objects::nonNull));
    }

    @Test
    void yamlPatternParsingTest() {
        String testPath = Objects.requireNonNull(this.getClass()
                                                     .getClassLoader()
                                                     .getResource("patterns/pattern-specification.yaml")).getFile();
        Constructor patternSpecificationConstructor = new Constructor(PatternSpecfication.class);
        TypeDescription typeDescription = new TypeDescription(PatternSpecfication.class);
        typeDescription.addPropertyParameters("regions", RegionSpecification.class);
        typeDescription.addPropertyParameters("transformations", TransformationSpecification.class);
        typeDescription.addPropertyParameters("formulas", FormulaSpecification.class);
        patternSpecificationConstructor.addTypeDescription(typeDescription);
        Yaml yaml = new Yaml(patternSpecificationConstructor);
        PatternSpecfication spec = null;
        try {
            spec = yaml.load(new FileInputStream(new File(testPath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.assertNotNull(spec);
        Assert.assertNotNull(spec.getFormulas());
        Assert.assertNotNull(spec.getTransformations());
        Assert.assertNotNull(spec.getRegions());
    }
}
