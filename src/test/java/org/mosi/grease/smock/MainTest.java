package org.mosi.grease.smock;

import org.junit.jupiter.api.Test;
import org.mosi.grease.Main;

import java.io.IOException;

public class MainTest {

    @Test
    public void testMain() {
        String[] args = {"src/test/resources/testdata/data.csv", "src/test/resources/patterns/pattern-specification.yaml", "results.json"};
        try {
            Main.main(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
