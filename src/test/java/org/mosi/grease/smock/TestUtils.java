package org.mosi.grease.smock;

import org.junit.Assert;
import org.mosi.grease.smock.model.Keyframe;

import java.util.Map;
import java.util.Objects;

public class TestUtils {
    public static void assertKeyFrames(Map<Double, Keyframe> keyframeMap) {
        Assert.assertNotNull("Keyfrmaes is null", keyframeMap);
        Assert.assertTrue("There should be keyframes", keyframeMap.size() > 0);
        Assert.assertTrue("There should be no null keyframes", keyframeMap
                .entrySet()
                .stream()
                .allMatch(Objects::nonNull));
        Assert.assertTrue("There should be no null shapes", keyframeMap
                .entrySet()
                .stream()
                .map(Map.Entry::getValue).map(Keyframe::getShape).allMatch(Objects::nonNull));
    }
}
