package org.mosi.grease.smock.elements.checker;

import org.mosi.grease.smock.model.CheckingResult;
import org.mosi.grease.smock.model.GeneratedRepresentation;
import org.mosi.grease.smock.model.Pattern;

abstract public class PatternChecker<T extends GeneratedRepresentation> {

    public abstract CheckingResult checkResult(T generatedRepresentation, Pattern pattern);
}
