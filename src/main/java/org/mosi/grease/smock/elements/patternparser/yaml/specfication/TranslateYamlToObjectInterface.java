package org.mosi.grease.smock.elements.patternparser.yaml.specfication;

public interface TranslateYamlToObjectInterface<T> {
    T translateToObject();
}
