package org.mosi.grease.smock.elements.patternparser.yaml.specfication;

import org.mosi.grease.smock.model.Pattern;
import org.mosi.grease.smock.model.patternelement.Formula;
import org.mosi.grease.smock.model.patternelement.Region;
import org.mosi.grease.smock.model.patternelement.Transformation;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class PatternSpecfication implements TranslateYamlToObjectInterface<Pattern> {
    private List<RegionSpecification> regions;
    private List<TransformationSpecification> transformations;
    private List<FormulaSpecification> formulas;

    public List<FormulaSpecification> getFormulas() {
        return formulas;
    }

    public void setFormulas(List<FormulaSpecification> formulas) {
        this.formulas = formulas;
    }

    public List<TransformationSpecification> getTransformations() {
        return transformations;
    }

    public void setTransformations(List<TransformationSpecification> transformations) {
        this.transformations = transformations;
    }

    public List<RegionSpecification> getRegions() {
        return regions;
    }

    public void setRegions(List<RegionSpecification> regions) {
        this.regions = regions;
    }

    @Override
    public Pattern translateToObject() {
        List<Region> regions = this.regions.stream()
                                           .map(RegionSpecification::translateToObject)
                                           .collect(toList());
        List<Transformation> transformations = this.transformations.stream()
                                                                   .map(TransformationSpecification::translateToObject)
                                                                   .collect(toList());
        List<Formula> formulas = this.formulas.stream()
                                              .map(FormulaSpecification::translateToObject)
                                              .collect(toList());
        return new Pattern(regions, transformations, formulas);
    }
}
