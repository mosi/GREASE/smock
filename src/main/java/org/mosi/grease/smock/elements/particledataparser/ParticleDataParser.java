package org.mosi.grease.smock.elements.particledataparser;

import org.mosi.grease.smock.model.ParticleData;

import java.io.File;

abstract public class ParticleDataParser {

    public abstract ParticleData parseParticleData(File particleFile);


}
