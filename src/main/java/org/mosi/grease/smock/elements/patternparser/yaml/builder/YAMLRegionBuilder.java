package org.mosi.grease.smock.elements.patternparser.yaml.builder;

import org.mosi.grease.smock.elements.patternparser.yaml.specfication.KeyframeSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.PropertiesSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.RegionSpecification;
import org.mosi.grease.smock.model.Keyframe;
import org.mosi.grease.smock.model.Period;
import org.mosi.grease.smock.model.patternelement.Region;
import org.mosi.grease.smock.model.shape.AbstractShape;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class YAMLRegionBuilder {
    public static Region build(RegionSpecification regionSpecification) {
        if (regionSpecification.getName().equals(""))
            throw new RuntimeException("Name is missing for region specification");
        if (regionSpecification.getPeriod().size() == 0 || regionSpecification.getPeriod().size() > 2)
            throw new RuntimeException("Invalid period specification for region " + regionSpecification.getName());

        String name = regionSpecification.getName();
        Period period = Period.buildPeriod(regionSpecification.getPeriod());
        if (regionSpecification.getProperties() != null && regionSpecification.getKeyframes() == null) {
            regionSpecification.setKeyframes(new ArrayList<>());
            KeyframeSpecification beginKeyFrame = new KeyframeSpecification(period.getBegin(), regionSpecification
                    .getProperties());
            regionSpecification.getKeyframes().add(beginKeyFrame);
            KeyframeSpecification endKeyFrame = new KeyframeSpecification(period.getEnd(), regionSpecification
                    .getProperties());
            regionSpecification.getKeyframes().add(endKeyFrame);
        }
        Map<Double, Keyframe> keyframeMap = YAMLRegionBuilder.buildKeyframeMap(regionSpecification);
        PropertiesSpecification properties = regionSpecification.getProperties();
        List<String> without = new ArrayList<>();
        if (properties != null) {
            if (properties.getWithout() != null) without = properties.getWithout();
        }
        List<String> neighbours = regionSpecification.getNeighbours() == null ? new ArrayList<>() : regionSpecification.getNeighbours();
        Region region = new Region(name, keyframeMap, period, neighbours, without);
        return region;
    }

    private static Map<Double, Keyframe> buildKeyframeMap(RegionSpecification regionSpecification) {
        Map<Double, Keyframe> keyframeMap = new HashMap<>();
        for (KeyframeSpecification keyframeSpecification : regionSpecification.getKeyframes()) {
            AbstractShape shape = YAMLShapeBuilder.buildShape(regionSpecification, keyframeSpecification);
            Keyframe keyframe = keyframeSpecification.translateToObject();
            keyframe.setShape(shape);
            keyframeMap.put(keyframe.getBegin(), keyframe);
        }
        return keyframeMap;
    }
}
