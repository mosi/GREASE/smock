package org.mosi.grease.smock.elements.patternparser.yaml.specfication;

import org.mosi.grease.smock.elements.patternparser.yaml.builder.YAMLFormulaBuilder;
import org.mosi.grease.smock.model.patternelement.Formula;

import java.util.List;

public class FormulaSpecification implements TranslateYamlToObjectInterface<Formula> {
    private String name;
    private String logic;
    private List<String> targets;
    private String specification;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogic() {
        return logic;
    }

    public void setLogic(String logic) {
        this.logic = logic;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    @Override
    public Formula translateToObject() {
        return YAMLFormulaBuilder.buildFormula(this);
    }
}
