package org.mosi.grease.smock.elements.checker;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.mosi.grease.smock.logic.sstl.SSTLFormula;

import java.util.Map;

public class FormulaCheckingResult {
    private final Map<String, Boolean> targetResults;
    private final SSTLFormula formula;

    public FormulaCheckingResult(SSTLFormula formula, Map<String, Boolean> targetResults) {
        this.targetResults = targetResults;
        this.formula = formula;
    }


    public Boolean getOverallResult() {
        return targetResults.values().stream().reduce(Boolean::logicalAnd).orElseGet(null);
    }

    public Map<String, Boolean> getTargetResults() {
        return targetResults;
    }

    @Override
    public String toString() {
        return this.toJSON().toString();
    }

    public JsonObject toJSON() {
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("targetResults", gson.toJsonTree(this.targetResults));
        jsonObject.addProperty("formula", this.formula.getName());
        return jsonObject;
    }
}

