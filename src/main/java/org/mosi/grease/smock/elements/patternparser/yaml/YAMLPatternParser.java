package org.mosi.grease.smock.elements.patternparser.yaml;

import org.mosi.grease.smock.elements.patternparser.PatternParser;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.FormulaSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.PatternSpecfication;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.RegionSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.TransformationSpecification;
import org.mosi.grease.smock.model.Pattern;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class YAMLPatternParser extends PatternParser {
    @Override
    public Pattern parsePattern(File file) {
        Constructor patternSpecificationConstructor = new Constructor(PatternSpecfication.class);
        TypeDescription typeDescription = new TypeDescription(PatternSpecfication.class);
        typeDescription.addPropertyParameters("regions", RegionSpecification.class);
        typeDescription.addPropertyParameters("transformations", TransformationSpecification.class);
        typeDescription.addPropertyParameters("formulas", FormulaSpecification.class);

        Yaml yaml = new Yaml(patternSpecificationConstructor);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            PatternSpecfication specification = yaml.load(fileInputStream);
            return specification.translateToObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

}
