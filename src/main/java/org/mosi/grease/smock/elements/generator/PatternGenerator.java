package org.mosi.grease.smock.elements.generator;

import org.mosi.grease.smock.model.Pattern;
import org.mosi.grease.smock.model.TransformedRepresentation;

abstract public class PatternGenerator<T> {
    public abstract T generate(TransformedRepresentation transformedRepresentation,
                               Pattern pattern);
}
