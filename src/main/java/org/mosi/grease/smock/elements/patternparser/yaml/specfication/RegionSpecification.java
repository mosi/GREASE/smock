package org.mosi.grease.smock.elements.patternparser.yaml.specfication;

import org.mosi.grease.smock.elements.patternparser.yaml.builder.YAMLRegionBuilder;
import org.mosi.grease.smock.model.patternelement.Region;

import java.util.List;

public class RegionSpecification implements TranslateYamlToObjectInterface<Region> {
    private String name;
    private List<Double> period;
    private String shape;
    private List<KeyframeSpecification> keyframes;
    private PropertiesSpecification properties;
    private List<String> neighbours;

    public List<String> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<String> neighbours) {
        this.neighbours = neighbours;
    }

    public PropertiesSpecification getProperties() {
        return properties;
    }

    public void setProperties(
            PropertiesSpecification properties) {
        this.properties = properties;
    }

    public List<KeyframeSpecification> getKeyframes() {
        return keyframes;
    }

    public void setKeyframes(List<KeyframeSpecification> keyframes) {
        this.keyframes = keyframes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Double> getPeriod() {
        return period;
    }

    public void setPeriod(List<Double> period) {
        this.period = period;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    @Override
    public Region translateToObject() {
        return YAMLRegionBuilder.build(this);
    }
}
