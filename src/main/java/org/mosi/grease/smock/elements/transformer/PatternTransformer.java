package org.mosi.grease.smock.elements.transformer;


import org.mosi.grease.smock.model.NormalizedRepresentation;
import org.mosi.grease.smock.model.ParticleRecord;
import org.mosi.grease.smock.model.TransformedRepresentation;
import org.mosi.grease.smock.model.patternelement.Transformation;
import org.mosi.grease.smock.model.shape.ResolvedRegion;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class PatternTransformer {
    private final TransformedRepresentation representation;

    public PatternTransformer() {
        this.representation = new TransformedRepresentation();
    }

    public TransformedRepresentation transform(NormalizedRepresentation normalizedRepresentation,
                                               List<Transformation> transformations) {
        this.representation.setObservationTimes(new ArrayList<>(normalizedRepresentation.getObservationTimes()));
        this.representation.setResolvedRegionMap(normalizedRepresentation.getObservationTimeResolvedRegionsMap());
        for (Transformation transformation : transformations) {
            Set<ResolvedRegion> resolvedRegions = normalizedRepresentation.getResolvedRegions();
            for (ResolvedRegion resolvedRegion : resolvedRegions) {
                List<ParticleRecord> particlesInResolvedRegion = normalizedRepresentation.getParticles(resolvedRegion);
                try {
                    Object result = transformation.apply(resolvedRegion, particlesInResolvedRegion);
                    this.representation.addTransformationResult(transformation, resolvedRegion, result);
                } catch (Exception exception) {
                    String transformationName = transformation.getName();
                    String transformationCode = transformation.getCode();
                    String msg = String.format("Can't calculate transformation result for %s %s", transformationName, transformationCode);
                    throw new RuntimeException(msg, exception);
                }
            }
        }
        return this.representation;
    }
}
