package org.mosi.grease.smock.elements.patternparser.yaml.specfication;

import org.mosi.grease.smock.elements.patternparser.yaml.builder.YAMLTransformationBuilder;
import org.mosi.grease.smock.model.patternelement.Transformation;

public class TransformationSpecification implements TranslateYamlToObjectInterface<Transformation> {
    private String name;
    private String formula;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    @Override
    public Transformation translateToObject() {
        return YAMLTransformationBuilder.buildTransformation(this);
    }
}
