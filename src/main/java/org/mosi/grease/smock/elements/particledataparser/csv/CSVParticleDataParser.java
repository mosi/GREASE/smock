package org.mosi.grease.smock.elements.particledataparser.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.mosi.grease.smock.elements.particledataparser.ParticleDataParser;
import org.mosi.grease.smock.model.ParticleData;
import org.mosi.grease.smock.model.ParticleRecord;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class CSVParticleDataParser extends ParticleDataParser {
    private final Set<Integer> ids = new HashSet<>();
    private final Set<Double> lastObservationTimeIds = new HashSet<>();
    private Double lastObservationTime;

    @Override
    public ParticleData parseParticleData(File particleFile) {
        try {
            CSVParser csvParser = CSVParser.parse(particleFile, Charset.defaultCharset(),
                    CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreSurroundingSpaces());
            return this.makeParticleData(csvParser);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ParticleData();
    }

    private ParticleData makeParticleData(CSVParser csvParser) {
        ParticleData result = new ParticleData();
        for (CSVRecord csvRecord : csvParser) {
            ParticleRecord particleRecord = this.makeParticleRecord(csvRecord);
            this.ids.add(particleRecord.getId());
            result.addParticleRecord(particleRecord);
        }
        result.calculateAuxiallaryAttributes();
        return result;
    }

    private ParticleRecord makeParticleRecord(CSVRecord csvRecord) {
        Double x = null;
        Double y = null;
        Double z = null;
        Double time = null;
        Integer id = null;
        Map<String, String> customAttributeMap = new HashMap<>();
        List<String> headerNames = csvRecord.getParser().getHeaderNames();
        for (String headerName : headerNames) {
            switch (headerName) {
                case "x":
                    x = Double.parseDouble(csvRecord.get(headerName));
                    break;
                case "y":
                    y = Double.parseDouble(csvRecord.get(headerName));
                    break;
                case "z":
                    z = Double.parseDouble(csvRecord.get(headerName));
                    break;
                case "time":
                    time = Double.parseDouble(csvRecord.get(headerName));
                    break;
                case "id":
                    id = Integer.parseInt(csvRecord.get(headerName));
                    break;
                default:
                    customAttributeMap.put(headerName, csvRecord.get(headerName));
            }
        }
        if (id == null || x == null || y == null || time == null) {
            throw new RuntimeException("Invalid particle record from CSV" + csvRecord.toString());
        }
        ParticleRecord resultRecord = new ParticleRecord(id, x, y, z, time, customAttributeMap);

        return resultRecord;
    }

}
