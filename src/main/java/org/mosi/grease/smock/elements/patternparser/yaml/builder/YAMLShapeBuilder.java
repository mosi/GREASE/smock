package org.mosi.grease.smock.elements.patternparser.yaml.builder;

import org.mosi.grease.smock.elements.patternparser.yaml.specfication.KeyframeSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.PropertiesSpecification;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.RegionSpecification;
import org.mosi.grease.smock.model.Point;
import org.mosi.grease.smock.model.shape.*;


public class YAMLShapeBuilder {
    public static AbstractShape buildShape(RegionSpecification regionSpecification,
                                           KeyframeSpecification keyframeSpecification) {
        ShapeTypes shapeType = ShapeTypes.getShape(regionSpecification.getShape());
        if (shapeType == null) {
            throw new RuntimeException(String.format("Cant cet shape for region \"%s\"", regionSpecification.getName()));
        }
        AbstractShape result = null;
        switch (shapeType) {
            case CIRCLE:
                result = YAMLShapeBuilder.buildCircle(keyframeSpecification);
                break;
            case RECTANGLE:
                result = YAMLShapeBuilder.buildRectangle(keyframeSpecification);
                break;
            case SQUARE:
                result = YAMLShapeBuilder.buildSquare(keyframeSpecification);
                break;
        }
        PropertiesSpecification propertiesSpecification = regionSpecification.getProperties();
        if (propertiesSpecification != null) {
            if (propertiesSpecification.getReferencePoint() != null) {
                result.setReferencePoint(Point.buildPoint(propertiesSpecification.getReferencePoint()));
            }
        }
        return result;
    }

    private static Square buildSquare(KeyframeSpecification keyframeSpecification) {
        PropertiesSpecification properties = keyframeSpecification.getProperties();
        if (properties.getUpperLeftCorner() == null || properties.getWidth() == null) {
            throw new IllegalArgumentException("Cant build square");
        }
        Point upperLeftCorner = Point.buildPoint(properties.getUpperLeftCorner());
        double width = properties.getWidth();
        return new Square(upperLeftCorner, width);
    }

    private static Rectangle buildRectangle(KeyframeSpecification keyframeSpecification) {
        PropertiesSpecification properties = keyframeSpecification.getProperties();
        if (properties.getUpperLeftCorner() == null || properties.getWidth() == null || properties.getHeight() == null) {
            throw new IllegalArgumentException("Cant build square");
        }
        Point upperLeftCorner = Point.buildPoint(properties.getUpperLeftCorner());
        double width = properties.getWidth();
        double height = properties.getHeight();
        return new Rectangle(upperLeftCorner, height, width);
    }

    private static Circle buildCircle(KeyframeSpecification keyframeSpecification) throws IllegalArgumentException {
        PropertiesSpecification properties = keyframeSpecification.getProperties();
        if (properties.getRadius() == null || properties.getCenter() == null) {
            throw new IllegalArgumentException("Cant build circle");
        }
        double radius = properties.getRadius();
        Point center = Point.buildPoint(properties.getCenter());
        return new Circle(center, radius);
    }
}
