package org.mosi.grease.smock.elements.patternparser;

import org.mosi.grease.smock.model.Pattern;

import java.io.File;

abstract public class PatternParser {

    public abstract Pattern parsePattern(File file);


}
