package org.mosi.grease.smock.elements.patternparser.yaml.specfication;

import java.util.List;

public class PropertiesSpecification {
    //Generic Properties
    private List<Double> referencePoint;
    private List<String> without;

    // Square and Rectangle
    private List<Double> upperLeftCorner;
    private Double width;
    private Double height;
    // Circle
    private List<Double> center;
    private Double radius;

    public List<String> getWithout() {
        return without;
    }

    public void setWithout(List<String> without) {
        this.without = without;
    }

    public List<Double> getReferencePoint() {
        return referencePoint;
    }

    public void setReferencePoint(List<Double> referencePoint) {
        this.referencePoint = referencePoint;
    }

    public List<Double> getUpperLeftCorner() {
        return upperLeftCorner;
    }

    public void setUpperLeftCorner(List<Double> upperLeftCorner) {
        this.upperLeftCorner = upperLeftCorner;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public List<Double> getCenter() {
        return center;
    }

    public void setCenter(List<Double> center) {
        this.center = center;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }
}
