package org.mosi.grease.smock.elements.normalizer;

import org.apache.commons.lang3.tuple.Pair;
import org.mosi.grease.smock.model.Keyframe;
import org.mosi.grease.smock.model.NormalizedRepresentation;
import org.mosi.grease.smock.model.ParticleData;
import org.mosi.grease.smock.model.ParticleRecord;
import org.mosi.grease.smock.model.patternelement.Region;
import org.mosi.grease.smock.model.scale.Scale;
import org.mosi.grease.smock.model.shape.AbstractShape;
import org.mosi.grease.smock.model.shape.ResolvedRegion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PatternNormalizer {
    private final Scale scale;
    private final NormalizedRepresentation representation;

    public PatternNormalizer(Scale scale) {
        this.scale = scale;
        this.representation = new NormalizedRepresentation();
    }

    private void calculateExistingRegions(Set<Double> observationTimes,
                                          List<Region> regions) {
        for (Double observationTime : observationTimes) {
            List<Region> regionsAtObservationTime = regions.stream()
                                                           .filter(region -> region.getPeriod()
                                                                                   .isTimeIncluded(observationTime))
                                                           .collect(Collectors.toList());
            this.representation.addExistingRegionsAtObservationTime(observationTime, regionsAtObservationTime);
        }
    }

    public NormalizedRepresentation normalize(ParticleData particleData, List<Region> regions) {
        Set<Double> observationTimes = particleData.getObservationTimes();
        this.calculateExistingRegions(observationTimes, regions);
        this.resolveRegionKeyframes();
        this.calculateParticlesInResolvedRegions(particleData);
        return this.representation;
    }

    private void resolveRegionKeyframes() {
        Set<Double> observationTimes = this.representation.getObservationTimes();
        for (double observationTime : observationTimes) {
            List<Region> regionsAtObservationTime = this.representation.getRegionsAtObservationTime(observationTime);
            List<ResolvedRegion> resolvedRegions = regionsAtObservationTime.stream()
                                                                           .map(region -> this.resolveRegion(observationTime, region))
                                                                           .collect(Collectors.toList());
            this.addResolvedRegionNeighbours(resolvedRegions, regionsAtObservationTime);
            this.representation.addResolvedRegionsAtObservationTime(observationTime, resolvedRegions);
        }
    }

    private void addResolvedRegionNeighbours(List<ResolvedRegion> resolvedRegions,
                                             List<Region> regionsAtObservationTime) {
        Map<String, Region> regionMap = regionsAtObservationTime.stream()
                                                                .collect(Collectors.toMap(Region::getName, Function
                                                                        .identity()));
        Map<String, ResolvedRegion> resolvedRegionMap = resolvedRegions.stream()
                                                                       .collect(Collectors.toMap(ResolvedRegion::getName, Function
                                                                               .identity()));
        for (ResolvedRegion resolvedRegion : resolvedRegions) {
            Region correspondingRegion = regionMap.get(resolvedRegion.getName());
            List<String> neighbourNames = correspondingRegion.getNeighbours()
                                                             .size() == 0 ? new ArrayList<>(resolvedRegionMap.keySet()) : correspondingRegion
                    .getNeighbours();
            List<ResolvedRegion> neighbours = neighbourNames
                    .stream()
                    .map(resolvedRegionMap::get)
                    .collect(Collectors.toList());
            resolvedRegion.setNeighbours(neighbours);
        }
    }

    private ResolvedRegion resolveRegion(Double observationTime, Region region) {
        AbstractShape resolvedShape = this.resolveRegionToShape(observationTime, region);
        return new ResolvedRegion(region, observationTime, resolvedShape);
    }


    private void calculateParticlesInResolvedRegions(ParticleData particleData) {
        Set<Double> observationTimes = this.representation.getObservationTimes();
        for (Double observationTime : observationTimes) {
            List<ParticleRecord> particleRecords = particleData.getParticleRecordsAtObservationTime(observationTime);
            List<ResolvedRegion> resolvedRegions = this.representation.getResolvedRegionsAtObservationTime(observationTime);
            for (ResolvedRegion resolvedRegion : resolvedRegions) {
                List<ParticleRecord> particlesInResolvedRegion = particleRecords.stream()
                                                                                .filter(resolvedRegion::containsParticle)
                                                                                .collect(Collectors
                                                                                        .toList());
                this.representation.addParticlesForRegion(resolvedRegion, particlesInResolvedRegion);
            }
        }
    }


    private AbstractShape resolveRegionToShape(Double observationTime, Region r) {
        Pair<Keyframe, Keyframe> relevantKeyFrames = r.getKeyFramesForInterpolation(observationTime);
        Keyframe startKeyFrame = relevantKeyFrames.getLeft();
        Keyframe endKeyFrame = relevantKeyFrames.getRight();
        Scale scale = this.scale.domain(startKeyFrame.getBegin(), endKeyFrame.getBegin());
        return startKeyFrame.getShape().interpolate(endKeyFrame.getShape(), scale, observationTime);
    }
}
