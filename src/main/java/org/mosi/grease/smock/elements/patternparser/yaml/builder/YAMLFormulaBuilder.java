package org.mosi.grease.smock.elements.patternparser.yaml.builder;

import org.mosi.grease.smock.elements.patternparser.yaml.specfication.FormulaSpecification;
import org.mosi.grease.smock.logic.sstl.parser.SSTLFormulaBuilder;
import org.mosi.grease.smock.model.patternelement.Formula;

public class YAMLFormulaBuilder {
    public static Formula buildFormula(FormulaSpecification formulaSpecification) {
        if (formulaSpecification.getLogic().equals("sstl")) {
            return SSTLFormulaBuilder.buildFormula(formulaSpecification);
        }
        return null;
    }


}
