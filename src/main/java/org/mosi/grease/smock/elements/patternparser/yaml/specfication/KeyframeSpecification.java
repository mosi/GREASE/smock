package org.mosi.grease.smock.elements.patternparser.yaml.specfication;

import org.mosi.grease.smock.model.Keyframe;

public class KeyframeSpecification implements TranslateYamlToObjectInterface<Keyframe> {
    private Double begin;
    private PropertiesSpecification properties;

    public KeyframeSpecification() {
    }

    public KeyframeSpecification(Double begin,
                                 PropertiesSpecification properties) {
        this.begin = begin;
        this.properties = properties;
    }

    public Double getBegin() {
        return begin;
    }

    public void setBegin(Double begin) {
        this.begin = begin;
    }

    public PropertiesSpecification getProperties() {
        return properties;
    }

    public void setProperties(
            PropertiesSpecification properties) {
        this.properties = properties;
    }

    @Override
    public Keyframe translateToObject() {
        return new Keyframe(null, begin);
    }
}
