package org.mosi.grease.smock.elements.checker;

import org.mosi.grease.smock.logic.sstl.SSTLFormula;
import org.mosi.grease.smock.logic.sstl.SSTLModelChecker;
import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;
import org.mosi.grease.smock.model.CheckingResult;
import org.mosi.grease.smock.model.Pattern;
import org.mosi.grease.smock.model.patternelement.Formula;

import java.util.ArrayList;
import java.util.List;

public class SSTLPatternChecker extends PatternChecker<SSTLRepresentation> {
    @Override
    public CheckingResult checkResult(SSTLRepresentation generatedRepresentation, Pattern pattern) {
        SSTLModelChecker modelChecker = new SSTLModelChecker(generatedRepresentation);
        List<FormulaCheckingResult> formulaCheckingResults = new ArrayList<>();
        for (Formula formula : pattern.getFormulas()) {
            SSTLFormula sstlFormula = (SSTLFormula) formula;
            FormulaCheckingResult formulaCheckingResult = modelChecker.check(sstlFormula);
            formulaCheckingResults.add(formulaCheckingResult);
        }
        return new CheckingResult(formulaCheckingResults);
    }
}
