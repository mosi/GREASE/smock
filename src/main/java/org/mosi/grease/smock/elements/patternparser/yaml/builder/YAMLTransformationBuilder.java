package org.mosi.grease.smock.elements.patternparser.yaml.builder;

import org.mosi.grease.smock.elements.patternparser.yaml.specfication.TransformationSpecification;
import org.mosi.grease.smock.model.ParticleRecord;
import org.mosi.grease.smock.model.patternelement.Transformation;
import org.mosi.grease.smock.model.shape.ResolvedRegion;
import pl.joegreen.lambdaFromString.LambdaCreationException;
import pl.joegreen.lambdaFromString.LambdaFactory;
import pl.joegreen.lambdaFromString.TypeReference;

import java.util.List;
import java.util.function.BiFunction;

public class YAMLTransformationBuilder {
    private static final LambdaFactory factory = LambdaFactory.get();

    public static Transformation buildTransformation(TransformationSpecification transformationSpecification) {
        BiFunction<ResolvedRegion, List<ParticleRecord>, Object> transformationFunction = null;
        try {
            transformationFunction = factory.createLambda(transformationSpecification.getFormula(), new TypeReference<BiFunction<ResolvedRegion, List<ParticleRecord>, Object>>() {
            });
            return new Transformation(transformationFunction, transformationSpecification.getName(), transformationSpecification
                    .getFormula());
        } catch (LambdaCreationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
