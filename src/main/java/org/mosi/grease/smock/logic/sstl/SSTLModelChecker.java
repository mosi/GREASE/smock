package org.mosi.grease.smock.logic.sstl;

import org.mosi.grease.smock.elements.checker.FormulaCheckingResult;
import org.mosi.grease.smock.logic.sstl.parser.model.SSTLFormulaElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SSTLModelChecker {
    private final SSTLRepresentation sstlRepresentation;

    public SSTLModelChecker(SSTLRepresentation generatedRepresentation) {
        this.sstlRepresentation = generatedRepresentation;
    }

    public FormulaCheckingResult check(SSTLFormula formula) {
        SSTLFormulaElement formulaRoot = formula.getFormulaRoot();
        List<String> targets = formula.getTargets();
        Map<String, Boolean> targetResults = new HashMap<>();
        for (String target : targets) {
            Boolean targetResult = formulaRoot.evaluate(this.sstlRepresentation, 0d, target);
            targetResults.put(target, targetResult);
        }
        FormulaCheckingResult formulaCheckingResult = new FormulaCheckingResult(formula, targetResults);
        return formulaCheckingResult;
    }
}
