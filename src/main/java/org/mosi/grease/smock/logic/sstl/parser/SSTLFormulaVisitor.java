package org.mosi.grease.smock.logic.sstl.parser;

import org.antlr.v4.runtime.tree.ParseTree;
import org.mosi.grease.smock.logic.sstl.parser.antlr.sstlBaseVisitor;
import org.mosi.grease.smock.logic.sstl.parser.antlr.sstlParser;
import org.mosi.grease.smock.logic.sstl.parser.model.*;

import java.util.List;
import java.util.stream.Collectors;

public class SSTLFormulaVisitor extends sstlBaseVisitor<SSTLFormulaElement> {

    @Override
    public SSTLFormulaElement visitSstl(sstlParser.SstlContext ctx) {
        return this.visit(ctx.formula());

    }

    @Override
    public SSTLFormulaElement visitAndFormula(sstlParser.AndFormulaContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new AndFormula(leftHandSide, rightHandSide);
    }

    @Override
    public OrFormula visitOrFormula(sstlParser.OrFormulaContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new OrFormula(leftHandSide, rightHandSide);
    }

    @Override
    public UntilFormula visitUntilFormula(sstlParser.UntilFormulaContext ctx) {
        Interval interval = this.visitInterval(ctx.interval());
        SSTLFormulaElement leftHandSide = this.visitParenFormula(ctx.parenFormula(0));
        SSTLFormulaElement rightHandSide = this.visitParenFormula(ctx.parenFormula(0));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new UntilFormula(leftHandSide, rightHandSide, interval);
    }

    @Override
    public SSTLFormulaElement visitGloballyFormula(sstlParser.GloballyFormulaContext ctx) {
        Interval interval = this.visitInterval(ctx.interval());
        SSTLFormulaElement otherPart = this.visitParenFormula(ctx.parenFormula());
        return new AlwaysFormula(otherPart, interval);
    }

    @Override
    public SSTLFormulaElement visitForeverFormula(sstlParser.ForeverFormulaContext ctx) {
        Interval interval = this.visitInterval(ctx.interval());
        SSTLFormulaElement otherPart = this.visitParenFormula(ctx.parenFormula());
        return new EventuallyFormula(otherPart, interval);
    }

    @Override
    public SSTLFormulaElement visitBoundedSurroundFormula(sstlParser.BoundedSurroundFormulaContext ctx) {
        Interval interval = this.visitInterval(ctx.interval());
        SSTLFormulaElement leftHandSide = this.visitParenFormula(ctx.parenFormula(0));
        SSTLFormulaElement rightHandSide = this.visitParenFormula(ctx.parenFormula(0));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new BoundedSurroundFormula(leftHandSide, interval, rightHandSide);
    }

    @Override
    public SSTLFormulaElement visitSomewhereFormula(sstlParser.SomewhereFormulaContext ctx) {
        Interval interval = this.visitInterval(ctx.interval());
        SSTLFormulaElement formulaElement = this.visitParenFormula(ctx.parenFormula());
        return new SomewhereFormula(interval, formulaElement);
    }

    @Override
    public SSTLFormulaElement visitEverywhereFormula(sstlParser.EverywhereFormulaContext ctx) {
        Interval interval = this.visitInterval(ctx.interval());
        SSTLFormulaElement formulaElement = this.visitParenFormula(ctx.parenFormula());
        return new EverywhereFormula(interval, formulaElement);
    }

    @Override
    public NotFormula visitNotformula(sstlParser.NotformulaContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getText());
        }
        return new NotFormula(leftHandSide);
    }

    @Override
    public SSTLFormulaElement visitImplyFormula(sstlParser.ImplyFormulaContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new ImplyFormula(leftHandSide, rightHandSide);
    }

    @Override
    public SSTLFormulaElement visitParenFormulaF(sstlParser.ParenFormulaFContext ctx) {
        SSTLFormulaElement result = this.visitParenFormula(ctx.parenFormula());
        if (result == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.parenFormula());
        }
        return result;
    }

    @Override
    public SSTLFormulaElement visitParenFormula(sstlParser.ParenFormulaContext ctx) {
        SSTLFormulaElement result = this.visit(ctx.getChild(1));
        if (result == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(1).getText());
        }
        return result;
    }

    @Override
    public Interval visitInterval(sstlParser.IntervalContext ctx) {
        List<Double> doubleList = ctx.NUMBER()
                                     .stream()
                                     .map(ParseTree::getText)
                                     .map(Double::parseDouble)
                                     .collect(Collectors.toList());
        return Interval.buildInterval(doubleList);
    }

    @Override
    public PlusFormula visitPlusExpression(sstlParser.PlusExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new PlusFormula(leftHandSide, rightHandSide);
    }

    @Override
    public DivideFormula visitDivideExpression(sstlParser.DivideExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new DivideFormula(leftHandSide, rightHandSide);
    }

    @Override
    public MinusFormula visitMinusExpression(sstlParser.MinusExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new MinusFormula(leftHandSide, rightHandSide);
    }

    @Override
    public SSTLFormulaElement visitBaseExpressionExpression(sstlParser.BaseExpressionExpressionContext ctx) {
        SSTLFormulaElement result = super.visitBaseExpressionExpression(ctx);
        if (result == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getText());
        }
        return result;
    }

    @Override
    public TimesFormula visitTimesExpression(sstlParser.TimesExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new TimesFormula(leftHandSide, rightHandSide);
    }

    @Override
    public LessThanFormula visitLessThanRelativeExpression(sstlParser.LessThanRelativeExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new LessThanFormula(leftHandSide, rightHandSide);
    }

    @Override
    public LessThanOrEqualFormula visitLessThanOrEqualRelativeExpression(
            sstlParser.LessThanOrEqualRelativeExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new LessThanOrEqualFormula(leftHandSide, rightHandSide);
    }

    @Override
    public GreaterThanFormula visitGreaterThanRelativeExpression(sstlParser.GreaterThanRelativeExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new GreaterThanFormula(leftHandSide, rightHandSide);
    }

    @Override
    public EqualFormula visitEqualRelativeExpression(sstlParser.EqualRelativeExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new EqualFormula(leftHandSide, rightHandSide);
    }

    @Override
    public UnequalFormula visitUnequalRelativeExpression(sstlParser.UnequalRelativeExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new UnequalFormula(leftHandSide, rightHandSide);
    }

    @Override
    public GreaterThanOrEqualFormula visitGreaterThanOrEqualRelativeExpression(
            sstlParser.GreaterThanOrEqualRelativeExpressionContext ctx) {
        SSTLFormulaElement leftHandSide = this.visit(ctx.getChild(0));
        SSTLFormulaElement rightHandSide = this.visit(ctx.getChild(2));
        if (leftHandSide == null) {
            throw new RuntimeException("Cant parse  formula: " + ctx.getChild(0).getText());
        } else if (rightHandSide == null) {
            throw new RuntimeException("Cant parse formula: " + ctx.getChild(2).getText());
        }
        return new GreaterThanOrEqualFormula(leftHandSide, rightHandSide);
    }

    @Override
    public SSTLVariable visitVariable(sstlParser.VariableContext ctx) {
        return new SSTLVariable(ctx.getText());
    }

    @Override
    public SSTLNumber visitNumber(sstlParser.NumberContext ctx) {
        return new SSTLNumber(Double.parseDouble(ctx.getText()));
    }

}
