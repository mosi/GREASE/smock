package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class NotFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement leftHandSide;

    public NotFormula(SSTLFormulaElement leftHandSide) {
        this.leftHandSide = leftHandSide;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        boolean innerValue = this.leftHandSide.evaluate(sstlRepresentation, time, locationName);
        return !innerValue;
    }
}
