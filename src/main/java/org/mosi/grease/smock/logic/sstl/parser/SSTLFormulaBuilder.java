package org.mosi.grease.smock.logic.sstl.parser;

import org.antlr.v4.runtime.*;
import org.mosi.grease.smock.elements.patternparser.yaml.specfication.FormulaSpecification;
import org.mosi.grease.smock.logic.sstl.SSTLFormula;
import org.mosi.grease.smock.logic.sstl.parser.antlr.sstlLexer;
import org.mosi.grease.smock.logic.sstl.parser.antlr.sstlParser;
import org.mosi.grease.smock.logic.sstl.parser.model.SSTLFormulaElement;
import org.mosi.grease.smock.model.patternelement.Formula;

public class SSTLFormulaBuilder {
    public static Formula buildFormula(FormulaSpecification formulaSpecification) {
        sstlLexer lexer = new sstlLexer(CharStreams.fromString(formulaSpecification.getSpecification()));

        lexer.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line,
                                    int charPositionInLine, String msg, RecognitionException e) {
                throw new RuntimeException(e);
            }
        });

        CommonTokenStream tokens = new CommonTokenStream(lexer);
        sstlParser sstlParser = new sstlParser(tokens);
        sstlParser.getBuildParseTree();
        SSTLFormulaVisitor formulaVisitor = new SSTLFormulaVisitor();
        SSTLFormulaElement formulaRoot = formulaVisitor.visit(sstlParser.sstl());
        return new SSTLFormula(formulaSpecification.getName(), formulaSpecification.getTargets(), formulaRoot);
    }
}
