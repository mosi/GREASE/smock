package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class SSTLVariable extends SSTLFormulaElement {
    private final String name;

    public SSTLVariable(String name) {
        this.name = name;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public Double extractValue(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        Object value = sstlRepresentation.getResult(this.name, time, locationName);
        return Double.parseDouble(value.toString());
    }


}
