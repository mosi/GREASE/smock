package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class SSTLNumber extends SSTLFormulaElement {
    private final Double value;

    public SSTLNumber(Double value) {
        this.value = value;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public Double extractValue(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        return value;
    }
}
