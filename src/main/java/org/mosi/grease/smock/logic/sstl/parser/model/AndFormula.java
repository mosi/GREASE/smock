package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class AndFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement leftHandSide;
    private final SSTLFormulaElement rightHandSide;

    public AndFormula(SSTLFormulaElement leftHandSide, SSTLFormulaElement rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        boolean leftHandValue = leftHandSide.evaluate(sstlRepresentation, time, locationName);
        return leftHandValue && rightHandSide.evaluate(sstlRepresentation, time, locationName);

    }
}
