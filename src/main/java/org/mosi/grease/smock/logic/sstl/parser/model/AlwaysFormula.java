package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class AlwaysFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement formula;
    private final Interval interval;

    public AlwaysFormula(SSTLFormulaElement formula, Interval interval) {
        this.formula = formula;
        this.interval = interval;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        SSTLFormulaElement auxillaryFormula = new NotFormula(
                new EventuallyFormula(new NotFormula(this.formula), this.interval));
        return auxillaryFormula.evaluate(sstlRepresentation, time, locationName);
    }
}
