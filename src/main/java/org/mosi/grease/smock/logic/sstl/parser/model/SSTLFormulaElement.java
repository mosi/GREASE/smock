package org.mosi.grease.smock.logic.sstl.parser.model;

import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public abstract class SSTLFormulaElement {
    abstract public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName);

    public Double extractValue(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        return null;
    }
}
