package org.mosi.grease.smock.logic.sstl.parser.model;

import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

import java.util.List;

public class Interval extends SSTLFormulaElement {
    private final double begin;
    private final double end;

    public Interval(double begin, double end) {
        this.begin = begin;
        this.end = end;
    }

    public static Interval buildInterval(List<Double> doubleList) {
        if (doubleList.size() == 2) {
            return new Interval(doubleList.get(0), doubleList.get(1));
        }
        throw new RuntimeException("Cant build point from double list: " + doubleList);
    }

    public double getBegin() {
        return begin;
    }

    public double getEnd() {
        return end;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        throw new RuntimeException("Not implemented");
    }
}
