package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class EventuallyFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement formula;
    private final Interval interval;

    public EventuallyFormula(SSTLFormulaElement formula, Interval interval) {
        this.formula = formula;
        this.interval = interval;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        SSTLFormulaElement auxiallaryFormula = new UntilFormula(new BoolFormula(true), this.formula, this.interval);
        return auxiallaryFormula.evaluate(sstlRepresentation, time, locationName);
    }
}
