package org.mosi.grease.smock.logic.sstl.parser.model;

import org.apache.commons.lang3.tuple.Pair;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

import java.util.Set;
import java.util.stream.Collectors;

public class BoundedSurroundFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement leftHandSide;
    private final SSTLFormulaElement rightHandSide;
    private final Interval interval;

    public BoundedSurroundFormula(SSTLFormulaElement leftHandSide, Interval interval,
                                  SSTLFormulaElement rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
        this.interval = interval;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        Set<String> lL = this.calculateValidLocation(sstlRepresentation, time, locationName, 0d, interval.getEnd());
        lL.add(locationName);
        Set<String> lPrimes = lL.stream()
                                .filter(l -> leftHandSide.evaluate(sstlRepresentation, time, l))
                                .collect(Collectors.toSet());
        Set<String> boundary = this.calculateBoundary(sstlRepresentation, time, lPrimes);
        Set<String> otherLocations = this.calculateValidLocation(sstlRepresentation, time, locationName, interval.getBegin(), interval
                .getEnd());
        otherLocations.retainAll(boundary);
        for (String otherLocation : otherLocations) {
            if (rightHandSide.evaluate(sstlRepresentation, time, otherLocation)) {
                return true;
            }
        }
        return false;
    }

    private Set<String> calculateValidLocation(SSTLRepresentation sstlRepresentation, Double time,
                                               String locationName, double lowerBound, double upperBound) {

        Graph<String, DefaultWeightedEdge> graph = sstlRepresentation.getGraph(time);
        DijkstraShortestPath<String, DefaultWeightedEdge> dijkstraShortestPath = new DijkstraShortestPath<>(graph);
        ShortestPathAlgorithm.SingleSourcePaths<String, DefaultWeightedEdge> results = dijkstraShortestPath.getPaths(locationName);
        Set<String> verticies = results.getGraph().vertexSet();
        return verticies
                .stream()
                .filter(s -> !s.equals(locationName))
                .map(s -> Pair.of(s, results.getWeight(s)))
                .filter(pair -> lowerBound <= pair.getValue() && pair.getValue() <= upperBound)
                .map(Pair::getKey)
                .collect(Collectors.toSet());
    }

    private Set<String> calculateBoundary(SSTLRepresentation sstlRepresentation, Double time,
                                          Set<String> locations) {
        Graph<String, DefaultWeightedEdge> graph = sstlRepresentation.getGraph(time);
        Set<String> neighbours = locations.stream().map(location -> Graphs.neighborSetOf(graph, location))
                                          .flatMap(Set::stream).collect(Collectors.toSet());
        neighbours.removeAll(locations);
        return neighbours;
    }
}