package org.mosi.grease.smock.logic.sstl.parser.model;

import org.apache.commons.lang3.tuple.Pair;
import org.jgrapht.Graph;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SomewhereFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement formulaElement;
    private final Interval interval;

    public SomewhereFormula(Interval interval,
                            SSTLFormulaElement formulaElement) {
        this.formulaElement = formulaElement;
        this.interval = interval;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        List<String> validLocations = this.calculateValidLocation(sstlRepresentation, time, locationName);
        if (validLocations.size() == 0) return false;
        for (String validLocation : validLocations) {
            if (this.formulaElement.evaluate(sstlRepresentation, time, validLocation)) {
                return true;
            }
        }
        return false;

    }

    private List<String> calculateValidLocation(SSTLRepresentation sstlRepresentation, Double time,
                                                String locationName) {
        Double lowerBound = interval.getBegin();
        Double upperBound = interval.getEnd();
        Graph<String, DefaultWeightedEdge> graph = sstlRepresentation.getGraph(time);
        DijkstraShortestPath<String, DefaultWeightedEdge> dijkstraShortestPath = new DijkstraShortestPath<>(graph);
        ShortestPathAlgorithm.SingleSourcePaths<String, DefaultWeightedEdge> results = dijkstraShortestPath.getPaths(locationName);
        Set<String> verticies = results.getGraph().vertexSet();
        return verticies
                .stream()
                .filter(s -> !s.equals(locationName))
                .map(s -> Pair.of(s, results.getWeight(s)))
                .filter(pair -> lowerBound <= pair.getValue() && pair.getValue() <= upperBound)
                .map(Pair::getKey)
                .collect(Collectors.toList());
    }
}