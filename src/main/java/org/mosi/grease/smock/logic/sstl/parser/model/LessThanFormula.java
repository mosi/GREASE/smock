package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class LessThanFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement leftHandSide;
    private final SSTLFormulaElement rightHandSide;

    public LessThanFormula(SSTLFormulaElement leftHandSide, SSTLFormulaElement rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        Double leftHandValue = leftHandSide.extractValue(sstlRepresentation, time, locationName);
        Double rightHandValue = rightHandSide.extractValue(sstlRepresentation, time, locationName);
        return leftHandValue < rightHandValue;
    }
}
