package org.mosi.grease.smock.logic.sstl.parser.model;

import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class EverywhereFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement formulaElement;
    private final Interval interval;

    public EverywhereFormula(Interval interval,
                             SSTLFormulaElement formulaElement) {
        this.formulaElement = formulaElement;
        this.interval = interval;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        SSTLFormulaElement auxillaryFormula = new NotFormula(
                new SomewhereFormula(this.interval,
                        new NotFormula(this.formulaElement)));
        return auxillaryFormula.evaluate(sstlRepresentation, time, locationName);

    }
}