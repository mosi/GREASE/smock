package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class PlusFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement leftHandSide;
    private final SSTLFormulaElement rightHandSide;

    public PlusFormula(SSTLFormulaElement leftHandSide, SSTLFormulaElement rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public Double extractValue(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        return leftHandSide.extractValue(sstlRepresentation, time, locationName) + rightHandSide.extractValue(sstlRepresentation, time, locationName);
    }
}
