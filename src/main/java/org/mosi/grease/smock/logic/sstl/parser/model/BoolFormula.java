package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

public class BoolFormula extends SSTLFormulaElement {


    private final Boolean value;

    public BoolFormula(Boolean value) {
        this.value = value;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        return this.value;
    }
}
