package org.mosi.grease.smock.logic.sstl;

import org.mosi.grease.smock.logic.sstl.parser.model.SSTLFormulaElement;
import org.mosi.grease.smock.model.patternelement.Formula;

import java.util.List;

public class SSTLFormula extends Formula {
    private final SSTLFormulaElement formulaRoot;

    public SSTLFormula(String name, List<String> targets, SSTLFormulaElement formulaRoot) {
        super(name, targets);
        this.formulaRoot = formulaRoot;
    }

    @Override
    public boolean evaluate() {
        return false;
    }

    public SSTLFormulaElement getFormulaRoot() {
        return formulaRoot;
    }


}
