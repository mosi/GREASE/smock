package org.mosi.grease.smock.logic.sstl.parser.model;


import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;

import java.util.List;
import java.util.stream.Collectors;

public class UntilFormula extends SSTLFormulaElement {
    private final SSTLFormulaElement leftHandSide;
    private final SSTLFormulaElement rightHandSide;
    private final Interval interval;

    public UntilFormula(SSTLFormulaElement leftHandSide, SSTLFormulaElement rightHandSide, Interval interval) {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
        this.interval = interval;
    }

    @Override
    public boolean evaluate(SSTLRepresentation sstlRepresentation, Double time, String locationName) {
        Interval baseInterval = new Interval(time + this.interval.getBegin(), time + this.interval.getEnd());
        List<Double> tPrimes = sstlRepresentation.getObservationTimes()
                                                 .stream()
                                                 .filter(aDouble -> baseInterval.getBegin() <= aDouble || aDouble <= baseInterval
                                                         .getEnd())
                                                 .collect(Collectors.toList());
        boolean outerResult = false;
        for (Double tPrime : tPrimes) {
            boolean rightHandValue = rightHandSide.evaluate(sstlRepresentation, tPrime, locationName);
            if (rightHandValue) {
                List<Double> tPrimePrimes = sstlRepresentation.getObservationTimes()
                                                              .stream()
                                                              .filter(aDouble -> time <= aDouble || time <= tPrime)
                                                              .collect(Collectors.toList());
                boolean innerResult = true;
                for (Double tPrimePrime : tPrimePrimes) {
                    boolean leftHandResult = leftHandSide.evaluate(sstlRepresentation, tPrimePrime, locationName);
                    if (!leftHandResult) {
                        innerResult = false;
                        break;
                    }
                }
                if (innerResult) {
                    outerResult = true;
                    break;
                }
            }
        }
        return outerResult;
    }
}
