package org.mosi.grease.smock.logic.sstl;

import org.apache.commons.lang3.tuple.Pair;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.mosi.grease.smock.model.GeneratedRepresentation;

import java.util.List;
import java.util.Map;

public class SSTLRepresentation extends GeneratedRepresentation {
    private final Map<String, Map<Pair<Double, String>, Object>> transformationResultsMap;
    private final List<Double> observationTimes;
    private final Map<Double, Graph<String, DefaultWeightedEdge>> graphMap;

    public SSTLRepresentation(
            Map<String, Map<Pair<Double, String>, Object>> transformationResultsMap,
            List<Double> observationTimes,
            Map<Double, Graph<String, DefaultWeightedEdge>> graphMap) {
        this.transformationResultsMap = transformationResultsMap;
        this.observationTimes = observationTimes;
        this.graphMap = graphMap;
    }

    public Map<Pair<Double, String>, Object> getTransformationResults(String transformationName) {
        return this.transformationResultsMap.get(transformationName);
    }

    public List<Double> getObservationTimes() {
        return observationTimes;
    }

    public Object getResult(String transformationName, Double observationTime, String regionName) {
        return this.getTransformationResults(transformationName).get(Pair.of(observationTime, regionName));
    }

    public Graph<String, DefaultWeightedEdge> getGraph(Double observationTime) {
        return this.graphMap.get(observationTime);
    }

}
