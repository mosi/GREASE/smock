package org.mosi.grease.smock.logic.sstl;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;
import org.mosi.grease.smock.elements.generator.PatternGenerator;
import org.mosi.grease.smock.model.Pattern;
import org.mosi.grease.smock.model.Point;
import org.mosi.grease.smock.model.TransformedRepresentation;
import org.mosi.grease.smock.model.shape.ResolvedRegion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SSTLPatternGenerator extends PatternGenerator<SSTLRepresentation> {
    // Time x Location -> W
    @Override
    public SSTLRepresentation generate(TransformedRepresentation transformedRepresentation, Pattern pattern) {
        Map<Double, Graph<String, DefaultWeightedEdge>> graphMap = this.buildSSTLGraphMap(transformedRepresentation);
        return new SSTLRepresentation(transformedRepresentation.getTransformationResultsMap(),
                transformedRepresentation.getObservationTimes(), graphMap);
    }

    private Map<Double, Graph<String, DefaultWeightedEdge>> buildSSTLGraphMap(
            TransformedRepresentation transformedRepresentation) {
        Map<Double, Graph<String, DefaultWeightedEdge>> graphMap = new HashMap<>();
        List<Double> observationTimes = transformedRepresentation.getObservationTimes();
        for (Double observationTime : observationTimes) {
            Graph<String, DefaultWeightedEdge> sstlGraph = this.buildSSTLGraph(observationTime, transformedRepresentation);
            graphMap.put(observationTime, sstlGraph);
        }
        return graphMap;
    }

    private Graph<String, DefaultWeightedEdge> buildSSTLGraph(Double observationTime,
                                                              TransformedRepresentation transformedRepresentation) {
        Graph<String, DefaultWeightedEdge> graph = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);
        List<ResolvedRegion> resolvedRegions = transformedRepresentation.getResolvedRegions(observationTime);
        for (ResolvedRegion resolvedRegion : resolvedRegions) graph.addVertex(resolvedRegion.getName());
        for (ResolvedRegion begin : resolvedRegions) {
            for (ResolvedRegion end : begin.getNeighbours()) {
                Double distance = this.distance(begin.getShape()
                                                     .getReferencePoint(), end.getShape()
                                                                              .getReferencePoint());
                String startNode = begin.getName();
                String endNode = end.getName();
                Graphs.addEdge(graph, startNode, endNode, distance);

            }
        }
        return graph;
    }

    private Double distance(Point referencePoint, Point other) {
        return referencePoint.distanceBetween(other);
    }
}
