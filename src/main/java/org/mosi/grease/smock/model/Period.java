package org.mosi.grease.smock.model;

import java.util.List;

public class Period {
    private final double begin;
    private final double end;

    public Period(double begin, double end) {
        this.begin = begin;
        this.end = end;
    }

    public static Period buildPeriod(List<Double> doubleList) {
        if (doubleList.size() == 2) {
            return new Period(doubleList.get(0), doubleList.get(1));
        }
        throw new RuntimeException("Cant build point from double list: " + doubleList);
    }

    public boolean isTimeIncluded(double time) {
        return begin <= time && time <= end;
    }

    public double getBegin() {
        return begin;
    }

    public double getEnd() {
        return end;
    }

}
