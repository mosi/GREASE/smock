package org.mosi.grease.smock.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.mosi.grease.smock.elements.checker.FormulaCheckingResult;

import java.util.List;

public class CheckingResult {
    private final List<FormulaCheckingResult> formulaCheckingResults;

    public CheckingResult(List<FormulaCheckingResult> formulaCheckingResults) {
        this.formulaCheckingResults = formulaCheckingResults;
    }

    public void report() {
        System.out.println(this);
    }

    private JsonElement toJSON() {
        JsonArray array = new JsonArray();
        formulaCheckingResults.stream().forEach(result -> array.add(result.toJSON()));
        return array;
    }

    @Override
    public String toString() {
        return this.toJSON().toString();
    }
}
