package org.mosi.grease.smock.model;

import org.apache.commons.lang3.tuple.Pair;
import org.mosi.grease.smock.model.patternelement.Transformation;
import org.mosi.grease.smock.model.shape.ResolvedRegion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransformedRepresentation {
    private final Map<String, Map<Pair<Double, String>, Object>> transformationResultsMap;
    private Map<Double, List<ResolvedRegion>> resolvedRegionMap;
    private List<Double> observationTimes;

    public TransformedRepresentation() {
        this.transformationResultsMap = new HashMap<>();
    }

    public List<Double> getObservationTimes() {
        return observationTimes;
    }

    public void setObservationTimes(List<Double> observationTimes) {
        this.observationTimes = observationTimes;
        this.observationTimes.sort(Double::compareTo);

    }

    public Map<String, Map<Pair<Double, String>, Object>> getTransformationResultsMap() {
        return transformationResultsMap;
    }

    public void addTransformationResult(Transformation transformation, ResolvedRegion resolvedRegion, Object result) {
        String transformationName = transformation.getName();
        Map<Pair<Double, String>, Object> transformationResults = transformationResultsMap.computeIfAbsent(transformationName, s -> new HashMap<>());
        Pair<Double, String> transformationEntryKey = Pair.of(resolvedRegion.getBegin(), resolvedRegion.getName());
        transformationResults.put(transformationEntryKey, result);
    }

    public void setResolvedRegionMap(
            Map<Double, List<ResolvedRegion>> resolvedRegionMap) {
        this.resolvedRegionMap = resolvedRegionMap;
    }

    public List<ResolvedRegion> getResolvedRegions(Double observationTime) {
        return this.resolvedRegionMap.get(observationTime);
    }
}
