package org.mosi.grease.smock.model.patternelement;

import org.apache.commons.lang3.tuple.Pair;
import org.mosi.grease.smock.model.Keyframe;
import org.mosi.grease.smock.model.Period;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Region {
    private final List<String> neighbours;
    private final List<String> without;
    private String name;
    private Period period;
    private Map<Double, Keyframe> keyframeMap;

    public Region(String name, Map<Double, Keyframe> keyframeMap, Period period,
                  List<String> neighbours, List<String> without) {
        this.keyframeMap = keyframeMap;
        this.name = name;
        this.period = period;
        this.neighbours = neighbours;
        this.without = without;
    }

    public List<String> getWithout() {
        return without;
    }

    public List<String> getNeighbours() {
        return neighbours;
    }

    public Pair<Keyframe, Keyframe> getKeyFramesForInterpolation(double time) {
        Set<Double> keyFrameTimes = keyframeMap.keySet();
        Double lowerTime = keyFrameTimes.stream()
                                        .filter(keyFrameTime -> keyFrameTime <= time)
                                        .max(Double::compareTo)
                                        .orElseThrow(RuntimeException::new);
        Double upperTime = keyFrameTimes.stream()
                                        .filter(keyFrameTime -> time < keyFrameTime)
                                        .min(Double::compareTo)
                                        .orElseThrow(() -> new RuntimeException("Cant interpolate keyframe for region " + this
                                                .getName()));
        Keyframe lowerKeyFrame = this.keyframeMap.get(lowerTime);
        Keyframe upperKeyFrame = this.keyframeMap.get(upperTime);
        return Pair.of(lowerKeyFrame, upperKeyFrame);
    }

    public Map<Double, Keyframe> getKeyframeMap() {
        return keyframeMap;
    }

    public void setKeyframeMap(Map<Double, Keyframe> keyframeMap) {
        this.keyframeMap = keyframeMap;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }
}
