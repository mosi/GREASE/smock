package org.mosi.grease.smock.model.scale;

public class LinearScale extends Scale {

    @Override
    public double getValue(double value) {
        double m = (this.rangeEnd - this.rangeStart) / (this.domainEnd - domainStart);
        double n = this.rangeEnd - m * this.domainEnd;
        return m * value + n;
    }
}
