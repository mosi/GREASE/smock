package org.mosi.grease.smock.model.shape;


import org.mosi.grease.smock.model.NormalizedRepresentation;
import org.mosi.grease.smock.model.ParticleRecord;
import org.mosi.grease.smock.model.patternelement.Region;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ResolvedRegion {
    private final List<String> without;
    private String name;
    private double begin;
    private AbstractShape shape;
    private NormalizedRepresentation normalizedRepresentation;
    private List<ResolvedRegion> neighbours;


    public ResolvedRegion(Region region, double begin, AbstractShape shape) {
        this.name = region.getName();
        this.begin = begin;
        this.shape = shape;
        this.without = region.getWithout();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, begin);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResolvedRegion that = (ResolvedRegion) o;
        return Double.compare(that.begin, begin) == 0 &&
                Objects.equals(name, that.name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBegin() {
        return begin;
    }

    public void setBegin(double begin) {
        this.begin = begin;
    }

    public AbstractShape getShape() {
        return shape;
    }

    public void setShape(AbstractShape shape) {
        this.shape = shape;
    }

    public boolean containsParticle(ParticleRecord particle) {
        Boolean isInOther = this.without.stream()
                                        .map(this::get)
                                        .map(r -> r.containsParticle(particle))
                                        .reduce(Boolean::logicalOr)
                                        .orElse(false);
        if (isInOther) return false;
        return this.shape.containsParticle(particle);
    }

    public ResolvedRegion get(String otherName) {
        return this.normalizedRepresentation.getOther(this, otherName);
    }

    public void setNormalizedRepresentation(NormalizedRepresentation normalizedRepresentation) {
        this.normalizedRepresentation = normalizedRepresentation;
    }

    public List<ParticleRecord> particles() {
        return this.normalizedRepresentation.getParticles(this);
    }

    public List<ResolvedRegion> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<ResolvedRegion> neighbours) {
        List<ResolvedRegion> filteredNeighbours = neighbours.stream()
                                                            .filter(resolvedRegion -> resolvedRegion != this)
                                                            .collect(Collectors.toList());
        this.neighbours = filteredNeighbours;
    }
}
