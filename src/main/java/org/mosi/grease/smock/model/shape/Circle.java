package org.mosi.grease.smock.model.shape;


import org.mosi.grease.smock.model.ParticleRecord;
import org.mosi.grease.smock.model.Point;
import org.mosi.grease.smock.model.scale.Scale;

public class Circle extends AbstractShape {
    private Point center;
    private double radius;

    public Circle(Point center, double radius) {
        super(center);
        this.center = center;
        this.radius = radius;
    }


    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public AbstractShape interpolate(AbstractShape otherShape, Scale scale, Double observationTime) {
        if (otherShape instanceof Circle) {
            Circle otherCircle = (Circle) otherShape;
            Point newCenter = this.center.interpolate(otherCircle.getCenter(), scale, observationTime);
            double newRadius = scale.range(this.radius, otherCircle.getRadius()).getValue(observationTime);
            AbstractShape shape = new Circle(newCenter, newRadius);
            if (this.getReferencePoint() == otherShape.getReferencePoint())
                shape.setReferencePoint(this.getReferencePoint());
            return shape;
        } else {
            throw new RuntimeException("Cant interpolate circle with other shape " + otherShape);
        }
    }

    @Override
    public boolean containsParticle(ParticleRecord particle) {
        return !(this.center.distanceBetween(particle.getLocation()) > this.radius);
    }
}
