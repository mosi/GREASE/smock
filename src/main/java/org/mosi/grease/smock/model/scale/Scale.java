package org.mosi.grease.smock.model.scale;

public abstract class Scale {
    protected double domainStart;
    protected double domainEnd;
    protected double rangeStart;
    protected double rangeEnd;

    public Scale domain(double domainStart, double domainEnd) {
        this.domainEnd = domainEnd;
        this.domainStart = domainStart;
        return this;
    }

    public Scale range(double rangeStart, double rangeEnd) {
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        return this;
    }

    public abstract double getValue(double value);


}
