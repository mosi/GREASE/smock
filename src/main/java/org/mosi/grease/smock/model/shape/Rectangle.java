package org.mosi.grease.smock.model.shape;


import org.mosi.grease.smock.model.ParticleRecord;
import org.mosi.grease.smock.model.Point;
import org.mosi.grease.smock.model.scale.Scale;

public class Rectangle extends AbstractShape {
    private double width;
    private double height;
    private Point upperLeftCorner;

    public Rectangle(Point upperLeftCorner, double height, double width) {
        super(new Point(upperLeftCorner.getX() + width / 2, upperLeftCorner.getY() - height / 2), upperLeftCorner);
        this.height = height;
        this.width = width;
        this.upperLeftCorner = upperLeftCorner;
    }


    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Point getUpperLeftCorner() {
        return upperLeftCorner;
    }

    public void setUpperLeftCorner(Point upperLeftCorner) {
        this.upperLeftCorner = upperLeftCorner;
    }

    @Override
    public AbstractShape interpolate(AbstractShape otherShape, Scale scale, Double observationTime) {
        if (otherShape instanceof Rectangle) {
            Rectangle otherRectangle = (Rectangle) otherShape;
            Point newUpperLeftCorner = this.upperLeftCorner.interpolate(otherRectangle.getUpperLeftCorner(), scale, observationTime);
            double newWidth = scale.range(this.width, otherRectangle.getWidth()).getValue(observationTime);
            double newHeight = scale.range(this.height, otherRectangle.getHeight()).getValue(observationTime);
            AbstractShape shape = new Rectangle(newUpperLeftCorner, newHeight, newWidth);
            if (this.getReferencePoint().equals(otherShape.getReferencePoint())) {
                shape.setReferencePoint(this.getReferencePoint());
            }
            return shape;
        } else {
            throw new RuntimeException("Cant interpolate circle with other shape " + otherShape);
        }
    }

    @Override
    public boolean containsParticle(ParticleRecord particle) {
        boolean inWidth = (particle.getX() >= upperLeftCorner.getX() && particle.getX() <= upperLeftCorner.getX() + width);
        boolean inHeight = (particle.getY() <= upperLeftCorner.getY() && particle.getY() >= upperLeftCorner.getY() - height);
        return inWidth && inHeight;
    }
}
