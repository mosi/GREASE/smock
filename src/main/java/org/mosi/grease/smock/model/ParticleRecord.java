package org.mosi.grease.smock.model;

import java.util.Map;

public class ParticleRecord {
    private final int id;
    private final double x;
    private final double y;
    private final double z;
    private final Double time;
    private final Map<String, String> attributes;

    public ParticleRecord(int id, double x, double y, double z, Double time, Map<String, String> attributes) {
        this.attributes = attributes;
        this.id = id;
        this.time = time;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getId() {
        return id;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Double getTime() {
        return time;
    }

    public Point getLocation() {
        return new Point(x, y);
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getAttribute(String attribute) {
        return this.attributes.get(attribute);
    }

    public void setAttribute(String attribute, String value) {
        this.attributes.put(attribute, value);
    }

    @Override
    public String toString() {
        return "ParticleRecord{" +
                "id=" + id +
                ", x=" + x +
                ", y=" + y +
                ", time=" + time +
                ", attributes=" + attributes +
                '}';
    }

    //    public Boolean isIn(GeometryAdapter geometryAdapter, String place){
//        return geometryAdapter.containsParticle(place,this,0);
//    }

//    public void setNewAttribute(){
//        this.attributes.remove("new");
//        this.attributes.put("new","1");
//    }
//
//    public void setLastFrameAttribute(){
//        this.attributes.remove("lastFrame");
//        this.attributes.put("lastFrame","1");
//    }
//
//    public void resetLastFrameAttribute(){
//        this.attributes.remove("lastFrame");
//        this.attributes.put("lastFrame","0");
//    }
}
