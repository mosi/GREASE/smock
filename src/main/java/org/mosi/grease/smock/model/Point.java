package org.mosi.grease.smock.model;

import org.mosi.grease.smock.model.scale.Scale;

import java.util.List;
import java.util.Objects;

public class Point {
    private final double x;
    private final double y;
    private final double z;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static Point buildPoint(List<Double> doubleList) {
        switch (doubleList.size()) {
            case 2:
                return new Point(doubleList.get(0), doubleList.get(1));
            case 3:
                return new Point(doubleList.get(0), doubleList.get(1), doubleList.get(2));
            default:
                throw new RuntimeException("Cant build point from double list: " + doubleList);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Double.compare(point.x, x) == 0 &&
                Double.compare(point.y, y) == 0 &&
                Double.compare(point.z, z) == 0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public Point interpolate(Point otherPoint, Scale scale, double observationTime) {
        double newX = scale.range(this.x, otherPoint.getX()).getValue(observationTime);
        double newY = scale.range(this.y, otherPoint.getY()).getValue(observationTime);
        double newZ = scale.range(this.z, otherPoint.getZ()).getValue(observationTime);
        return new Point(newX, newY, newZ);
    }

    public double distanceBetween(Point otherPoint) {
        return Math.sqrt(Math.pow(this.x - otherPoint.x, 2) + Math.pow(this.y - otherPoint.y, 2));
    }

}
