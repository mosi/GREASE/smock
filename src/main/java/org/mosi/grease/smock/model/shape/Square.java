package org.mosi.grease.smock.model.shape;

import org.mosi.grease.smock.model.Point;

public class Square extends Rectangle {
    public Square(Point upperLeftCorner, double width) {
        super(upperLeftCorner, width, width);
    }

}
