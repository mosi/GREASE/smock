package org.mosi.grease.smock.model.patternelement;

import org.mosi.grease.smock.model.ParticleRecord;
import org.mosi.grease.smock.model.shape.ResolvedRegion;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;

public class Transformation {
    private final BiFunction<ResolvedRegion, List<ParticleRecord>, Object> transformationFunction;
    private String name;
    private final String code;

    public Transformation(
            BiFunction<ResolvedRegion, List<ParticleRecord>, Object> transformationFunction, String name, String code) {
        this.transformationFunction = transformationFunction;
        this.name = name;
        this.code = code;
    }

    public BiFunction<ResolvedRegion, List<ParticleRecord>, Object> getTransformationFunction() {
        return transformationFunction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transformation that = (Transformation) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public Object apply(ResolvedRegion region, List<ParticleRecord> particleRecords) {
        Object result = this.transformationFunction.apply(region, particleRecords);
        return result;
    }
}
