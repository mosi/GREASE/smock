package org.mosi.grease.smock.model;

import org.mosi.grease.smock.model.patternelement.Formula;
import org.mosi.grease.smock.model.patternelement.Region;
import org.mosi.grease.smock.model.patternelement.Transformation;

import java.util.List;

public class Pattern {
    List<Region> regions;
    List<Transformation> transformations;
    List<Formula> formulas;

    public Pattern(List<Region> regions,
                   List<Transformation> transformations,
                   List<Formula> formulas) {
        this.regions = regions;
        this.transformations = transformations;
        this.formulas = formulas;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public List<Transformation> getTransformations() {
        return transformations;
    }

    public List<Formula> getFormulas() {
        return formulas;
    }
}
