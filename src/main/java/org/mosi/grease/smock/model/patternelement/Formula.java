package org.mosi.grease.smock.model.patternelement;

import java.util.List;

public abstract class Formula {
    private String name;
    private List<String> targets;

    public Formula(String name, List<String> targets) {
        this.name = name;
        this.targets = targets;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    abstract public boolean evaluate();

    @Override
    public String toString() {
        return "Formula{" +
                "name='" + name + '\'' +
                ", targets=" + targets +
                '}';
    }
}
