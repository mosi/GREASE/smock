package org.mosi.grease.smock.model;

import org.mosi.grease.smock.model.patternelement.Region;
import org.mosi.grease.smock.model.shape.ResolvedRegion;

import java.util.*;

public class NormalizedRepresentation {
    private final Set<Double> observationTimes;
    private final Map<Double, List<ResolvedRegion>> observationTimeResolvedRegionsMap;
    private final Map<Double, List<Region>> observationTimeRegionsMap;
    private final Map<ResolvedRegion, List<ParticleRecord>> resolvedRegionsParticleMap;


    public NormalizedRepresentation() {
        this.observationTimeResolvedRegionsMap = new HashMap<>();
        this.resolvedRegionsParticleMap = new HashMap<>();
        this.observationTimeRegionsMap = new HashMap<>();
        this.observationTimes = new HashSet<>();
    }

    public void addResolvedRegion(double observationTime, ResolvedRegion region) {
        observationTimeResolvedRegionsMap.computeIfAbsent(observationTime, aDouble -> new ArrayList<>()).add(region);
    }

    public void addParticlesForRegion(ResolvedRegion region, List<ParticleRecord> particleRecords) {
        this.resolvedRegionsParticleMap.put(region, particleRecords);
    }

    public Map<Double, List<ResolvedRegion>> getObservationTimeResolvedRegionsMap() {
        return observationTimeResolvedRegionsMap;
    }


    public Map<ResolvedRegion, List<ParticleRecord>> getResolvedRegionsParticleMap() {
        return resolvedRegionsParticleMap;
    }


    public void addExistingRegionsAtObservationTime(Double observationTime, List<Region> regions) {
        this.observationTimes.add(observationTime);
        this.observationTimeRegionsMap.put(observationTime, regions);
    }

    public Set<Double> getObservationTimes() {
        return observationTimes;
    }

    public Map<Double, List<Region>> getObservationTimeRegionsMap() {
        return observationTimeRegionsMap;
    }

    public List<Region> getRegionsAtObservationTime(double observationTime) {
        return this.observationTimeRegionsMap.get(observationTime);
    }

    public void addResolvedRegionsAtObservationTime(double observationTime, List<ResolvedRegion> resolvedRegions) {
        resolvedRegions.stream().forEach(resolvedRegion -> resolvedRegion.setNormalizedRepresentation(this));
        this.observationTimeResolvedRegionsMap.put(observationTime, resolvedRegions);
    }

    public List<ResolvedRegion> getResolvedRegionsAtObservationTime(Double observationTime) {
        return this.observationTimeResolvedRegionsMap.get(observationTime);
    }

    public Set<ResolvedRegion> getResolvedRegions() {
        return this.resolvedRegionsParticleMap.keySet();
    }

    public List<ParticleRecord> getParticles(ResolvedRegion resolvedRegion) {
        return this.resolvedRegionsParticleMap.get(resolvedRegion);
    }

    public ResolvedRegion getOther(ResolvedRegion resolvedRegion, String otherName) {
        List<ResolvedRegion> alternatives = this.observationTimeResolvedRegionsMap.get(resolvedRegion.getBegin());
        return alternatives.stream()
                           .filter(alternative -> alternative.getName().equals(otherName))
                           .findFirst()
                           .orElseThrow(RuntimeException::new);
    }
}
