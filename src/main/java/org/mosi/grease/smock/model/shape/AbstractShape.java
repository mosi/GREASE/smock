package org.mosi.grease.smock.model.shape;


import org.mosi.grease.smock.model.ParticleRecord;
import org.mosi.grease.smock.model.Point;
import org.mosi.grease.smock.model.scale.Scale;

public abstract class AbstractShape {
    private Point referencePoint;
    private Point coordinate;

    public AbstractShape(Point coordinate) {
        this.coordinate = coordinate;
        this.referencePoint = coordinate;
    }

    public AbstractShape(Point referencePoint, Point coordinate) {
        this.referencePoint = referencePoint;
        this.coordinate = coordinate;
    }

    public abstract AbstractShape interpolate(AbstractShape otherShape, Scale scale,
                                              Double observationTime);

    public Point getReferencePoint() {
        return referencePoint;
    }

    public void setReferencePoint(Point referencePoint) {
        this.referencePoint = referencePoint;
    }

    public Point getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Point coordinate) {
        this.coordinate = coordinate;
    }

    public abstract boolean containsParticle(ParticleRecord particle);
}
