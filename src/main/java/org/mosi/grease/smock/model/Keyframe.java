package org.mosi.grease.smock.model;

import org.mosi.grease.smock.model.shape.AbstractShape;

public class Keyframe {
    private AbstractShape shape;
    private Double begin;

    public Keyframe(AbstractShape shape, Double begin) {
        this.shape = shape;
        this.begin = begin;
    }

    public AbstractShape getShape() {
        return shape;
    }

    public void setShape(AbstractShape shape) {
        this.shape = shape;
    }

    public Double getBegin() {
        return begin;
    }

    public void setBegin(Double begin) {
        this.begin = begin;
    }
}
