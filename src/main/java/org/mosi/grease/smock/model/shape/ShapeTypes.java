package org.mosi.grease.smock.model.shape;

import java.util.Arrays;

public enum ShapeTypes {
    CIRCLE("Circle"),
    RECTANGLE("Rectangle"),
    SQUARE("Square");

    private final String shapeString;

    ShapeTypes(String shapeString) {
        this.shapeString = shapeString;
    }

    public static ShapeTypes getShape(String shapeString) {
        return Arrays.stream(values()).filter(value -> value.shapeString.equals(shapeString)).findFirst().orElse(null);
    }
}
