package org.mosi.grease.smock.model;


import java.util.*;
import java.util.stream.Collectors;

public class ParticleData {
    private final List<ParticleRecord> particleRecordList;
    private final Map<Double, List<ParticleRecord>> observationTimeMap;
    private final Set<Integer> ids;
    private final Set<Double> observationTimes;
    private final Map<Integer, ParticleRecord> lastParticleRecordMap;

    public ParticleData() {
        this.particleRecordList = new ArrayList<>();
        this.observationTimeMap = new HashMap<>();
        this.observationTimes = new HashSet<>();
        this.ids = new HashSet<>();
        this.lastParticleRecordMap = new HashMap<>();
    }

    public void addParticleRecord(ParticleRecord particleRecord) {
        if (particleRecord.getTime() == null) {
            throw new RuntimeException("Time for particle record is missing: " + particleRecord);
        }
        this.observationTimeMap.computeIfAbsent(particleRecord.getTime(), aDouble -> new ArrayList<>())
                               .add(particleRecord);
        this.lastParticleRecordMap.put(particleRecord.getId(), particleRecord);
        this.particleRecordList.add(particleRecord);
        this.observationTimes.add(particleRecord.getTime());
        this.ids.add(particleRecord.getId());
        particleRecord.setAttribute("new", "false");
        particleRecord.setAttribute("last", "false");

    }


    public Set<Double> getObservationTimes() {
        return this.observationTimes;
    }

    public List<ParticleRecord> getParticleRecordsAtObservationTime(Double observationTime) {
        if (!this.observationTimeMap.containsKey(observationTime)) {
            throw new RuntimeException("Cant find particle records at observation time " + observationTime);
        }
        return this.observationTimeMap.get(observationTime);
    }

    public void calculateAuxiallaryAttributes() {
        this.calculateNewAttribute();
        this.calculateLastAttribute();
    }

    private void calculateLastAttribute() {
        Double lastObservationTime = this.observationTimes.stream().max(Double::compareTo).get();
        for (Map.Entry<Integer, ParticleRecord> entry : this.lastParticleRecordMap.entrySet()) {
            if (!entry.getValue().getTime().equals(lastObservationTime)) {
                entry.getValue()
                     .setAttribute("last", "true");
            }
        }

    }

    private void calculateNewAttribute() {
        Set<Integer> ids = new HashSet<>();
        List<Double> sortedObservationTimes = observationTimes.stream()
                                                              .sorted(Double::compareTo)
                                                              .collect(Collectors.toList());
        for (Double observationTime : sortedObservationTimes) {
            List<ParticleRecord> records = this.getParticleRecordsAtObservationTime(observationTime);
            for (ParticleRecord record : records) {
                if (ids.contains(record.getId())) {
                    record.setAttribute("new", "false");
                } else {
                    record.setAttribute("new", "true");
                    ids.add(record.getId());
                }
            }
        }
    }
}
