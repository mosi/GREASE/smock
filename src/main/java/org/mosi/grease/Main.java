package org.mosi.grease;


import org.mosi.grease.smock.elements.checker.PatternChecker;
import org.mosi.grease.smock.elements.checker.SSTLPatternChecker;
import org.mosi.grease.smock.elements.generator.PatternGenerator;
import org.mosi.grease.smock.elements.normalizer.PatternNormalizer;
import org.mosi.grease.smock.elements.particledataparser.ParticleDataParser;
import org.mosi.grease.smock.elements.particledataparser.csv.CSVParticleDataParser;
import org.mosi.grease.smock.elements.patternparser.PatternParser;
import org.mosi.grease.smock.elements.patternparser.yaml.YAMLPatternParser;
import org.mosi.grease.smock.elements.transformer.PatternTransformer;
import org.mosi.grease.smock.logic.sstl.SSTLPatternGenerator;
import org.mosi.grease.smock.logic.sstl.SSTLRepresentation;
import org.mosi.grease.smock.model.*;
import org.mosi.grease.smock.model.scale.LinearScale;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        File particleFile = new File(args[0]);
        File patternFile = new File(args[1]);
        File resultFile = null;
        if (args.length > 2) {
            resultFile = new File(args[2]);
            resultFile.createNewFile();

        }

        ParticleDataParser particleDataParser = new CSVParticleDataParser();
        PatternParser patternParser = new YAMLPatternParser();
        PatternNormalizer patternNormalizer = new PatternNormalizer(new LinearScale());
        PatternTransformer patternTransformer = new PatternTransformer();
        PatternGenerator<SSTLRepresentation> patternGenerator = new SSTLPatternGenerator();
        PatternChecker<SSTLRepresentation> patternChecker = new SSTLPatternChecker();

        Pattern pattern = patternParser.parsePattern(patternFile);
        ParticleData particleData = particleDataParser.parseParticleData(particleFile);
        NormalizedRepresentation normalizedRepresentation = patternNormalizer.normalize(particleData, pattern.getRegions());
        TransformedRepresentation transformedRepresentation = patternTransformer.transform(normalizedRepresentation, pattern
                .getTransformations());
        SSTLRepresentation generatedRepresentation = patternGenerator.generate(transformedRepresentation, pattern);
        CheckingResult checkingResult = patternChecker.checkResult(generatedRepresentation, pattern);
        checkingResult.report();
        if (resultFile != null) {
            BufferedWriter writer = new BufferedWriter(new FileWriter(resultFile));
            writer.write(checkingResult.toString());
            writer.close();
        }
    }


}
