grammar sstl;

sstl
    : formula EOF;

formula
    : parenFormula 'and' parenFormula # AndFormula
    | parenFormula 'or' parenFormula # OrFormula
    | parenFormula 'U' interval parenFormula # UntilFormula
    | 'G' interval parenFormula # GloballyFormula
    | 'F' interval parenFormula # ForeverFormula
    | parenFormula 'S' interval parenFormula # BoundedSurroundFormula
    | '<<>>' interval parenFormula # SomewhereFormula
    | '[[]]' interval parenFormula # EverywhereFormula
    | '!' parenFormula # Notformula
    | parenFormula '->' parenFormula  # ImplyFormula
    | parenFormula # parenFormulaF
    ;


parenFormula
    : '(' relativeExpression ')'
    | '(' formula ')';

interval
    : '[' NUMBER ',' NUMBER ']';

expression
    : baseExpression # BaseExpressionExpression
    | expression '+' expression # PlusExpression
    | expression '-' expression # MinusExpression
    | expression '/' expression # DivideExpression
    | expression '*' expression # TimesExpression
    ;

relativeExpression
    : expression '<' expression # LessThanRelativeExpression
    | expression '<=' expression # LessThanOrEqualRelativeExpression
    | expression '>' expression # GreaterThanRelativeExpression
    | expression '==' expression # EqualRelativeExpression
    | expression '!=' expression # UnequalRelativeExpression
    | expression '>=' expression # GreaterThanOrEqualRelativeExpression
    ;

baseExpression
    : VAR # Variable
    | NUMBER # Number
    ;

NUMBER
    : DIGIT+
    | DIGIT+ '.' DIGIT+;

fragment DIGIT
    : ('0'..'9');
VAR
    : [a-zA-Z]+;
WS
    : [ \r\n\t] + -> skip;